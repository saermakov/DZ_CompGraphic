import os
from mutagen.mp3 import MP3

#Меняем формат первого и второго видео из mkv в mp4 и извлекаем из них аудидорожку.
os.system('ffmpeg -i input1.mkv input1.mp4')
os.system('ffmpeg -i input2.mkv input2.mp4')
os.system('ffmpeg -i input1.mp4 -vn -ar 44100 -ac 2 -ab 129K -f mp3 sound_video1.mp3')
os.system('ffmpeg -i input2.mp4 -vn -ar 44100 -ac 2 -ab 129K -f mp3 sound_video2.mp3')

#Находим с помощью модуля mutagen длину обоих видео.
lenght_video1 = MP3('sound_video1.mp3').info.length
lenght_video2 = MP3('sound_video2.mp3').info.length

#Сравниваем длины первого и второго видео в секундах и урезаем либо видео, либо аудидорожку.
#Далее извлекаем аудиодорожку из input1.mp4 и заменяем её на аудиодорожку из видео input2.mp4.
if lenght_video1 > lenght_video2:
    os.system('ffmpeg -i sound_video1.mp3 -c:a copy -ss 00:00:00 -t ' + str(lenght_video2) + ' sound_video1_cropped.mp3')
    os.system('ffmpeg -i input2.mp4 -c copy -an input2_without_sound.mp4')
    os.system('ffmpeg -i input2_without_sound.mp4 -i sound_video1_cropped.mp3 -c copy input2_with_sound.mp4')
    os.remove('sound_video1_cropped.mp3')
else:
    os.system('ffmpeg -i input2.mp4 -c:v copy -ss 00:00:00 -t ' + str(lenght_video1) + ' input2_cropped.mp4')
    os.system('ffmpeg -i input2_cropped.mp4 -c copy -an input2_without_sound.mp4')
    os.system('ffmpeg -i input2_without_sound.mp4 -i sound_video1.mp3 -c copy input2_with_sound.mp4')
    os.remove('input2_cropped.mp3')

#Конвертируем видео в h.265 и h.264 с нужными нам параметрами.
os.system('ffmpeg -i input2_with_sound.mp4 -c:v libx265 -b:v 12M -minrate 12M'
          ' -maxrate 12M  -bufsize 12M -r 48 -aspect 16:9 -ar 96000 output1.mp4')
os.system('ffmpeg -i input2_with_sound.mp4 -c:v libx264 -b:v 12M -minrate 12M'
          ' -maxrate 12M  -bufsize 12M -r 48 -aspect 16:9 -ar 96000 output2.mp4')

#Удаляем ненужные нам файлы.
os.remove('input1.mp4')
os.remove('input2.mp4')
os.remove('sound_video1.mp3')
os.remove('sound_video2.mp3')
os.remove('input2_without_sound.mp4')
os.remove('input2_with_sound.mp4')

#С помощью os.stat находим размеры наших файлов в байтах.
#Также находим разницу размеров файлов в байтах.
stats_output1 = os.stat('output1.mp4')
size_output1 = stats_output1.st_size
stats_output2 = os.stat('output2.mp4')
size_output2 = stats_output2.st_size
difference_size = abs(size_output1 - size_output2)

#Сравниваем размеры, находим больший и записываем это.
if size_output1 > size_output2:
    os.system("echo file size of output1.mp4 is more than file size output2.mp4")
else:
    os.system("echo file size of output2.mp4 is more than file size output1.mp4")
os.system("echo difference in bytes : " + str(difference_size) + " bytes")
